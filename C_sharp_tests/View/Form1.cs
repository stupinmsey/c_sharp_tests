﻿using C_sharp_tests.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C_sharp_tests.Presenter;
using System.Diagnostics;

namespace C_sharp_tests
{
    public partial class MainForm : Form, IForm
    {
        public MainLogicPresenter Presenter { private get; set; }

        public string ConsoleLabel
        {
            get { return this.consoleLabel.Text; }
            set { this.consoleLabel.Text = value; }
        }

        public Color ConsoleColor
        {
            set { this.BackColor = value; }
        }

        public bool CompleteTestButton_Enabled
        {
            get { return completeTestButton.Enabled & completeTestButton.Visible; }
            set { completeTestButton.Enabled = completeTestButton.Visible = value; }
        }
        public bool PrevQuestionButton_Enabled
        {
            get { return previousQuestionButton.Enabled & previousQuestionButton.Visible; }
            set { previousQuestionButton.Enabled = previousQuestionButton.Visible = value; }
        }
        public bool NextQuestionButton_Enabled
        {
            get { return nextQuestionButton.Enabled & nextQuestionButton.Visible; }
            set { nextQuestionButton.Enabled = nextQuestionButton.Visible = value; }
        }

        public bool MenuButtonsEnabled
        {
            get
            {
                return this.JuniorMenuButton.Enabled & this.MiddleMenuButton.Enabled & this.SeniorMenuButton.Enabled & this.MixedMenuButton.Enabled &
                    this.JuniorMenuButton.Visible & this.MiddleMenuButton.Visible & this.SeniorMenuButton.Visible & this.MixedMenuButton.Visible;
            }

            set
            {
                this.JuniorMenuButton.Enabled = this.MiddleMenuButton.Enabled = this.SeniorMenuButton.Enabled = this.MixedMenuButton.Enabled =
                    this.JuniorMenuButton.Visible = this.MiddleMenuButton.Visible = this.SeniorMenuButton.Visible = this.MixedMenuButton.Visible = value;
            }
        }


        public string ScreenAddress
        {
            set { Screen.Image = Image.FromFile(value); }
        }

        public bool ScreenVisible
        {
            set { Screen.Enabled = Screen.Visible = value; }
        }

        public bool AllRadioButtons_Enabled
        {
            get
            {
                return this.RadioButton1_Enabled & this.RadioButton2_Enabled & this.RadioButton3_Enabled &
                    this.RadioButton4_Enabled & this.RadioButton5_Enabled & this.RadioButton6_Enabled;
            }

            set
            {
                this.RadioButton1_Enabled = this.RadioButton2_Enabled = this.RadioButton3_Enabled =
                     this.RadioButton4_Enabled = this.RadioButton5_Enabled = this.RadioButton6_Enabled = value;
            }
        }

        public bool RadioButton1_Enabled
        {
            get { return radioButton1.Enabled & radioButton1.Visible; }
            set { radioButton1.Enabled = radioButton1.Visible = value; }
        }
        public bool RadioButton2_Enabled
        {
            get { return radioButton2.Enabled & radioButton2.Visible; }
            set { radioButton2.Enabled = radioButton2.Visible = value; }
        }
        public bool RadioButton3_Enabled
        {
            get { return radioButton3.Enabled & radioButton3.Visible; }
            set { radioButton3.Enabled = radioButton3.Visible = value; }
        }
        public bool RadioButton4_Enabled
        {
            get { return radioButton4.Enabled & radioButton4.Visible; }
            set { radioButton4.Enabled = radioButton4.Visible = value; }
        }
        public bool RadioButton5_Enabled
        {
            get { return radioButton5.Enabled & radioButton5.Visible; }
            set { radioButton5.Enabled = radioButton5.Visible = value; }
        }
        public bool RadioButton6_Enabled
        {
            get { return radioButton6.Enabled & radioButton6.Visible; }
            set { radioButton6.Enabled = radioButton6.Visible = value; }
        }

        public bool AllToggleButtons_Enabled
        {
            get
            {
                return this.ToggleButton1_Enabled & this.ToggleButton2_Enabled & this.ToggleButton3_Enabled &
                       this.ToggleButton4_Enabled & this.ToggleButton5_Enabled & this.ToggleButton6_Enabled;
            }

            set
            {
                this.ToggleButton1_Enabled = this.ToggleButton2_Enabled = this.ToggleButton3_Enabled =
                this.ToggleButton4_Enabled = this.ToggleButton5_Enabled = this.ToggleButton6_Enabled = value;
            }
        }

        public bool ToggleButton1_Enabled
        {
            get { return radioButton1.Enabled & radioButton1.Visible; }
            set { checkBox1.Enabled = checkBox1.Visible = value; }
        }
        public bool ToggleButton2_Enabled
        {
            get { return checkBox2.Enabled & checkBox2.Visible; }
            set { checkBox2.Enabled = checkBox2.Visible = value; }
        }
        public bool ToggleButton3_Enabled
        {
            get { return checkBox3.Enabled & checkBox3.Visible; }
            set { checkBox3.Enabled = checkBox3.Visible = value; }
        }
        public bool ToggleButton4_Enabled
        {
            get { return checkBox4.Enabled & checkBox4.Visible; }
            set { checkBox4.Enabled = checkBox4.Visible = value; }
        }
        public bool ToggleButton5_Enabled
        {
            get { return checkBox5.Enabled & checkBox5.Visible; }
            set { checkBox5.Enabled = checkBox5.Visible = value; }
        }
        public bool ToggleButton6_Enabled
        {
            get { return checkBox6.Enabled & checkBox6.Visible; }
            set { checkBox6.Enabled = checkBox6.Visible = value; }
        }

        public string QuestionContent
        {
            set { this.questionContent.Text = value; }
        }

        public string RadioButton1_Text { set { this.radioButton1.Text = value; } }

        public string RadioButton2_Text { set { this.radioButton2.Text = value; } }

        public string RadioButton3_Text { set { this.radioButton3.Text = value; } }

        public string RadioButton4_Text { set { this.radioButton4.Text = value; } }

        public string RadioButton5_Text { set { this.radioButton5.Text = value; } }

        public string RadioButton6_Text { set { this.radioButton6.Text = value; } }

        public string ToggleButton1_Text { set { this.checkBox1.Text = value; } }

        public string ToggleButton2_Text { set { this.checkBox2.Text = value; } }

        public string ToggleButton3_Text { set { this.checkBox3.Text = value; } }

        public string ToggleButton4_Text { set { this.checkBox4.Text = value; } }

        public string ToggleButton5_Text { set { this.checkBox5.Text = value; } }

        public string ToggleButton6_Text { set { this.checkBox6.Text = value; } }
    
        public MainForm()
        {
            InitializeComponent();            
        }

        private async void MainForm_Load(object sender, EventArgs e)
        {
            await Presenter.LaunchStartLoader();
            Presenter.ShowStartMenu();
        }

        private void JuniorMenuButton_Click(object sender, EventArgs e)
        {
            Presenter.LaunchTest(MainLogicPresenter.TestTypes.junior);
        }

        private void MiddleMenuButton_Click(object sender, EventArgs e)
        {
            Presenter.LaunchTest(MainLogicPresenter.TestTypes.middle);
        }

        private void SeniorMenuButton_Click(object sender, EventArgs e)
        {
            Presenter.LaunchTest(MainLogicPresenter.TestTypes.senior);
        }

        private void MixedMenuButton_Click(object sender, EventArgs e)
        {
            Presenter.LaunchTest(MainLogicPresenter.TestTypes.mixed);
        }

        private void nextQuestionButton_Click(object sender, EventArgs e)
        {
            Presenter.SwitchNextQuestion();
        }

        private void previousQuestionButton_Click(object sender, EventArgs e)
        {
            Presenter.SwitchPrevQuestion();
        }
    }
}

