﻿using C_sharp_tests.Presenter;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_sharp_tests.View
{
    public interface IForm
    {
        string ConsoleLabel { get; set; }
        Color ConsoleColor { set; }
        
        string QuestionContent {set; }

        bool MenuButtonsEnabled { get; set; }

        bool CompleteTestButton_Enabled { get; set; }
        bool PrevQuestionButton_Enabled { get; set; }
        bool NextQuestionButton_Enabled { get; set; }

        bool AllRadioButtons_Enabled { get; set; }
        bool RadioButton1_Enabled { get; set; }
        bool RadioButton2_Enabled { get; set; }
        bool RadioButton3_Enabled { get; set; }
        bool RadioButton4_Enabled { get; set; }
        bool RadioButton5_Enabled { get; set; }
        bool RadioButton6_Enabled { get; set; }

        bool AllToggleButtons_Enabled { get; set; }
        bool ToggleButton1_Enabled { get; set; }
        bool ToggleButton2_Enabled { get; set; }
        bool ToggleButton3_Enabled { get; set; }
        bool ToggleButton4_Enabled { get; set; }
        bool ToggleButton5_Enabled { get; set; }
        bool ToggleButton6_Enabled { get; set; }

        string RadioButton1_Text { set; }
        string RadioButton2_Text { set; }
        string RadioButton3_Text { set; }
        string RadioButton4_Text { set; }
        string RadioButton5_Text { set; }
        string RadioButton6_Text {  set; }
        
        string ToggleButton1_Text {  set; }
        string ToggleButton2_Text {  set; }
        string ToggleButton3_Text { set; }
        string ToggleButton4_Text {  set; }
        string ToggleButton5_Text {  set; }
        string ToggleButton6_Text {  set; }


        string ScreenAddress { set; }
        bool ScreenVisible { set; }

        MainLogicPresenter Presenter {set;}
    }
}
