﻿namespace C_sharp_tests
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.consoleLabel = new System.Windows.Forms.Label();
            this.JuniorMenuButton = new System.Windows.Forms.Button();
            this.MiddleMenuButton = new System.Windows.Forms.Button();
            this.SeniorMenuButton = new System.Windows.Forms.Button();
            this.MixedMenuButton = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.Screen = new System.Windows.Forms.PictureBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.nextQuestionButton = new System.Windows.Forms.Button();
            this.previousQuestionButton = new System.Windows.Forms.Button();
            this.completeTestButton = new System.Windows.Forms.Button();
            this.questionContent = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).BeginInit();
            this.SuspendLayout();
            // 
            // consoleLabel
            // 
            this.consoleLabel.CausesValidation = false;
            this.consoleLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.consoleLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.consoleLabel.Location = new System.Drawing.Point(12, 12);
            this.consoleLabel.Name = "consoleLabel";
            this.consoleLabel.Size = new System.Drawing.Size(660, 340);
            this.consoleLabel.TabIndex = 0;
            // 
            // JuniorMenuButton
            // 
            this.JuniorMenuButton.Enabled = false;
            this.JuniorMenuButton.Location = new System.Drawing.Point(597, 8);
            this.JuniorMenuButton.Name = "JuniorMenuButton";
            this.JuniorMenuButton.Size = new System.Drawing.Size(75, 23);
            this.JuniorMenuButton.TabIndex = 1;
            this.JuniorMenuButton.Text = "Junior";
            this.JuniorMenuButton.UseVisualStyleBackColor = true;
            this.JuniorMenuButton.Visible = false;
            this.JuniorMenuButton.Click += new System.EventHandler(this.JuniorMenuButton_Click);
            // 
            // MiddleMenuButton
            // 
            this.MiddleMenuButton.Enabled = false;
            this.MiddleMenuButton.Location = new System.Drawing.Point(597, 37);
            this.MiddleMenuButton.Name = "MiddleMenuButton";
            this.MiddleMenuButton.Size = new System.Drawing.Size(75, 23);
            this.MiddleMenuButton.TabIndex = 2;
            this.MiddleMenuButton.Text = "Middle";
            this.MiddleMenuButton.UseVisualStyleBackColor = true;
            this.MiddleMenuButton.Visible = false;
            this.MiddleMenuButton.Click += new System.EventHandler(this.MiddleMenuButton_Click);
            // 
            // SeniorMenuButton
            // 
            this.SeniorMenuButton.Enabled = false;
            this.SeniorMenuButton.Location = new System.Drawing.Point(597, 66);
            this.SeniorMenuButton.Name = "SeniorMenuButton";
            this.SeniorMenuButton.Size = new System.Drawing.Size(75, 23);
            this.SeniorMenuButton.TabIndex = 3;
            this.SeniorMenuButton.Text = "Senior";
            this.SeniorMenuButton.UseVisualStyleBackColor = true;
            this.SeniorMenuButton.Visible = false;
            this.SeniorMenuButton.Click += new System.EventHandler(this.SeniorMenuButton_Click);
            // 
            // MixedMenuButton
            // 
            this.MixedMenuButton.Enabled = false;
            this.MixedMenuButton.Location = new System.Drawing.Point(597, 95);
            this.MixedMenuButton.Name = "MixedMenuButton";
            this.MixedMenuButton.Size = new System.Drawing.Size(75, 23);
            this.MixedMenuButton.TabIndex = 4;
            this.MixedMenuButton.Text = "Mixed";
            this.MixedMenuButton.UseVisualStyleBackColor = true;
            this.MixedMenuButton.Visible = false;
            this.MixedMenuButton.Click += new System.EventHandler(this.MixedMenuButton_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Location = new System.Drawing.Point(24, 353);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(80, 17);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Enabled = false;
            this.checkBox2.Location = new System.Drawing.Point(24, 376);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(80, 17);
            this.checkBox2.TabIndex = 6;
            this.checkBox2.Text = "checkBox2";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Visible = false;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Enabled = false;
            this.checkBox3.Location = new System.Drawing.Point(24, 399);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(80, 17);
            this.checkBox3.TabIndex = 7;
            this.checkBox3.Text = "checkBox3";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.Visible = false;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Enabled = false;
            this.checkBox4.Location = new System.Drawing.Point(24, 422);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(80, 17);
            this.checkBox4.TabIndex = 8;
            this.checkBox4.Text = "checkBox4";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.Visible = false;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Enabled = false;
            this.checkBox5.Location = new System.Drawing.Point(24, 445);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(80, 17);
            this.checkBox5.TabIndex = 9;
            this.checkBox5.Text = "checkBox5";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.Visible = false;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Enabled = false;
            this.checkBox6.Location = new System.Drawing.Point(24, 468);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(80, 17);
            this.checkBox6.TabIndex = 10;
            this.checkBox6.Text = "checkBox6";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.Visible = false;
            // 
            // Screen
            // 
            this.Screen.Enabled = false;
            this.Screen.Location = new System.Drawing.Point(31, 12);
            this.Screen.Name = "Screen";
            this.Screen.Size = new System.Drawing.Size(549, 297);
            this.Screen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Screen.TabIndex = 11;
            this.Screen.TabStop = false;
            this.Screen.Visible = false;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Enabled = false;
            this.radioButton1.Location = new System.Drawing.Point(121, 353);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(85, 17);
            this.radioButton1.TabIndex = 12;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "radioButton1";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.Visible = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Enabled = false;
            this.radioButton2.Location = new System.Drawing.Point(121, 376);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(85, 17);
            this.radioButton2.TabIndex = 13;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "radioButton2";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.Visible = false;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Enabled = false;
            this.radioButton3.Location = new System.Drawing.Point(121, 399);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(85, 17);
            this.radioButton3.TabIndex = 14;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "radioButton3";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.Visible = false;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Enabled = false;
            this.radioButton4.Location = new System.Drawing.Point(121, 422);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(85, 17);
            this.radioButton4.TabIndex = 15;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "radioButton4";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.Visible = false;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Enabled = false;
            this.radioButton5.Location = new System.Drawing.Point(121, 444);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(85, 17);
            this.radioButton5.TabIndex = 16;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "radioButton5";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.Visible = false;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Enabled = false;
            this.radioButton6.Location = new System.Drawing.Point(121, 468);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(85, 17);
            this.radioButton6.TabIndex = 17;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "radioButton6";
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.Visible = false;
            // 
            // nextQuestionButton
            // 
            this.nextQuestionButton.Enabled = false;
            this.nextQuestionButton.Location = new System.Drawing.Point(586, 159);
            this.nextQuestionButton.Name = "nextQuestionButton";
            this.nextQuestionButton.Size = new System.Drawing.Size(20, 23);
            this.nextQuestionButton.TabIndex = 18;
            this.nextQuestionButton.Text = ">";
            this.nextQuestionButton.UseVisualStyleBackColor = true;
            this.nextQuestionButton.Visible = false;
            this.nextQuestionButton.Click += new System.EventHandler(this.nextQuestionButton_Click);
            // 
            // previousQuestionButton
            // 
            this.previousQuestionButton.Enabled = false;
            this.previousQuestionButton.Location = new System.Drawing.Point(5, 159);
            this.previousQuestionButton.Name = "previousQuestionButton";
            this.previousQuestionButton.Size = new System.Drawing.Size(20, 23);
            this.previousQuestionButton.TabIndex = 19;
            this.previousQuestionButton.Text = "<";
            this.previousQuestionButton.UseVisualStyleBackColor = true;
            this.previousQuestionButton.Visible = false;
            this.previousQuestionButton.Click += new System.EventHandler(this.previousQuestionButton_Click);
            // 
            // completeTestButton
            // 
            this.completeTestButton.Enabled = false;
            this.completeTestButton.Location = new System.Drawing.Point(417, 399);
            this.completeTestButton.Name = "completeTestButton";
            this.completeTestButton.Size = new System.Drawing.Size(75, 23);
            this.completeTestButton.TabIndex = 20;
            this.completeTestButton.Text = "Complete";
            this.completeTestButton.UseVisualStyleBackColor = true;
            this.completeTestButton.Visible = false;
            // 
            // questionContent
            // 
            this.questionContent.AutoSize = true;
            this.questionContent.Location = new System.Drawing.Point(28, 312);
            this.questionContent.Name = "questionContent";
            this.questionContent.Size = new System.Drawing.Size(35, 13);
            this.questionContent.TabIndex = 21;
            this.questionContent.Text = "label1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(684, 489);
            this.Controls.Add(this.questionContent);
            this.Controls.Add(this.completeTestButton);
            this.Controls.Add(this.previousQuestionButton);
            this.Controls.Add(this.nextQuestionButton);
            this.Controls.Add(this.radioButton6);
            this.Controls.Add(this.radioButton5);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.Screen);
            this.Controls.Add(this.checkBox6);
            this.Controls.Add(this.checkBox5);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.MixedMenuButton);
            this.Controls.Add(this.SeniorMenuButton);
            this.Controls.Add(this.MiddleMenuButton);
            this.Controls.Add(this.JuniorMenuButton);
            this.Controls.Add(this.consoleLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "C# Tests 1.0";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label consoleLabel;
        private System.Windows.Forms.Button JuniorMenuButton;
        private System.Windows.Forms.Button MiddleMenuButton;
        private System.Windows.Forms.Button SeniorMenuButton;
        private System.Windows.Forms.Button MixedMenuButton;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.PictureBox Screen;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.Button nextQuestionButton;
        private System.Windows.Forms.Button previousQuestionButton;
        private System.Windows.Forms.Button completeTestButton;
        private System.Windows.Forms.Label questionContent;
    }
}

