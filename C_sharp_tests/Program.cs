﻿using C_sharp_tests.Model;
using C_sharp_tests.Presenter;
using System;
using System.Windows.Forms;

namespace C_sharp_tests
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var view = new MainForm();
            var loader = new OnStartLoader();
            var test = Test.Instance;
            var presenter = new MainLogicPresenter(view, test , loader);
            Application.Run(view);
        }
    }
}
