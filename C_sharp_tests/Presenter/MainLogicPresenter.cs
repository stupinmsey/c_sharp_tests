﻿using C_sharp_tests.Model.Interfaces;
using C_sharp_tests.View;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace C_sharp_tests.Presenter
{
    public class MainLogicPresenter
    {
        private readonly IForm _view;
        private readonly IStartLoader _loader;
        private readonly ITest _test;

        public enum TestTypes
        {
            junior =1,
            middle,
            senior,
            mixed
        }


        public MainLogicPresenter(IForm view, ITest test, IStartLoader loader)
        {
            _view = view;
            _view.Presenter = this;
            _loader = loader;
            _test = test;
        }


        public async Task<bool> LaunchStartLoader()
        {
            _view.ConsoleColor = _loader.ColorBlack;

            for (int i = 0; i <= _loader.MessageAmount; i++)
            {
                await Task.Delay(400);
                _view.ConsoleLabel += _loader.GetMessage;
            }
            _view.ConsoleColor = _loader.ColorWhite;
            return true;
        }


        public void ShowStartMenu()
        {
            _view.MenuButtonsEnabled = true;
           // _view.ScreenAddress = @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\small.jpg";
        }

        public void LaunchTest(TestTypes testType)
        {
            _test.Initialize(testType);
            _view.ScreenVisible =/* _view.AllRadioButtons_Enabled = */_view.AllToggleButtons_Enabled = _view.CompleteTestButton_Enabled
                                = _view.NextQuestionButton_Enabled = _view.PrevQuestionButton_Enabled= true;
            _view.MenuButtonsEnabled = false;
            UpdateContent();
        }

        public void SwitchNextQuestion()
        {
            _test.NextQuestion();
            UpdateContent();
        }

        public void SwitchPrevQuestion()
        {
            _test.PreviousQuestion();
            UpdateContent();
        }


        void UpdateContent()
        {
            _view.ScreenAddress = _test.GetQuestionImage();
            _view.QuestionContent = _test.GetQuestion();
            _view.RadioButton1_Text = _test.GetAnswer(0);
            _view.RadioButton2_Text = _test.GetAnswer(1);
            _view.RadioButton3_Text = _test.GetAnswer(2);
            _view.RadioButton4_Text = _test.GetAnswer(3);
            _view.RadioButton5_Text = _test.GetAnswer(4);
            _view.RadioButton6_Text = _test.GetAnswer(5);

            _view.ToggleButton1_Text = _test.GetAnswer(0);
            _view.ToggleButton2_Text = _test.GetAnswer(1);
            _view.ToggleButton3_Text = _test.GetAnswer(2);
            _view.ToggleButton4_Text = _test.GetAnswer(3);
            _view.ToggleButton5_Text = _test.GetAnswer(4);
            _view.ToggleButton6_Text = _test.GetAnswer(5);
        }
    }
}
