﻿#define DEBUG

using C_sharp_tests.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using static C_sharp_tests.Presenter.MainLogicPresenter;


namespace C_sharp_tests.Model
{
    public class Test : ITest
    {
        #region Singletone

        private static Test instance = null;

        private Test()
        {
#if DEBUG
            Debug.WriteLine("new Test");
#endif
        }

        public static Test Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Test();
                }
                return instance;
            }
        }

        #endregion

        private const sbyte JUNIOR = 5;
        private const sbyte MIDDLE = 5;
        private const sbyte SENIOR = 5;
        private const sbyte MIXED = 9;

        private List<Question> questions;

        private static int currentQuestionIndex;


        public void Initialize(TestTypes level)
        {
            switch (level)
            {
                case TestTypes.junior:
                    {
#if DEBUG
                        Debug.WriteLine(level);
#endif
                        GenerateQuestions(JUNIOR);
                    }
                    break;
                case TestTypes.middle:
                    {
#if DEBUG
                        Debug.WriteLine(level);
#endif
                        GenerateQuestions(MIDDLE);
                    }
                    break;
                case TestTypes.senior:
                    {
#if DEBUG
                        Debug.WriteLine(level);
#endif
                        GenerateQuestions(SENIOR);
                    }
                    break;
                case TestTypes.mixed:
                    {
#if DEBUG
                        Debug.WriteLine(level);
#endif
                        GenerateQuestions(MIXED);
                    }
                    break;
            }
        }

        private void GenerateQuestions(sbyte amount)
        {
            currentQuestionIndex = 0;
            questions = new List<Question>();
            Random rnd = new Random();
            int random = rnd.Next(1, Question.NUM_OF_QUESTIONS);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            for (sbyte i = 0; i <= amount; i++)
            {
                while (questions.Any(q => q.GetCurrentQuestionIndex == random)) random = rnd.Next(1, Question.NUM_OF_QUESTIONS);
                questions.Add(new Question(random));
            }

            sw.Stop();
            Debug.WriteLine(questions.Count);
            Debug.WriteLine(sw.Elapsed);
        }



        public string GetQuestionImage()
        {
            return questions[currentQuestionIndex].GetCurrentImage;
        }

        public string GetQuestion()
        {
            return questions[currentQuestionIndex].GetCurrentQuestion;
        }

        public string GetAnswer(int answerNumber)
        {
            return questions[currentQuestionIndex].GetCurrentAnswer(answerNumber);
        }

        public void SelectQuestion()
        {
            //
        }

        public void NextQuestion()
        {
            if (currentQuestionIndex< questions.Count-1) currentQuestionIndex++;            
        }

        public void PreviousQuestion()
        {
            if (currentQuestionIndex > 0) currentQuestionIndex--;
        }

    }

}
    

