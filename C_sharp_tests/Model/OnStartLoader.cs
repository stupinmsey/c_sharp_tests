﻿using C_sharp_tests.Model.Interfaces;
using System.Collections.Generic;
using System;

namespace C_sharp_tests.Model
{
    class OnStartLoader : IStartLoader
    {

        public int MessageAmount { get { return numOfMessages; } }
        private static int numOfMessages;
        private static string[] Messages =
            {
            "Initializing . . .\n",
            "Getting proccess data . . .\n",
            "Completed!\n"
            };

        public System.Drawing.Color ColorWhite
        {
            get { return System.Drawing.Color.White; }
        }
            public System.Drawing.Color ColorBlack
        {
            get { return System.Drawing.Color.Black; }
        }


        private static string GetMessages()
        {
            if (numOfMessages>=3) return string.Empty;                        
            return Messages[numOfMessages++];
        }

        public string GetMessage
        {
            get { return GetMessages(); }
        }


    }
}
