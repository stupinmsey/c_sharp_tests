﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C_sharp_tests.Presenter;

namespace C_sharp_tests.Model.Interfaces
{
    public interface ITest
    {
       void Initialize(MainLogicPresenter.TestTypes testType);
        string GetQuestionImage();
        string GetQuestion();
        string GetAnswer(int answerNumber);
        void SelectQuestion();
        void NextQuestion();
        void PreviousQuestion();
    }
}
