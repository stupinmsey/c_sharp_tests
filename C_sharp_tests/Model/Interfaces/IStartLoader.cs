﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_sharp_tests.Model.Interfaces
{
    public interface IStartLoader
    {
        int MessageAmount { get; }
        string GetMessage { get; }
        System.Drawing.Color ColorWhite { get; }
        System.Drawing.Color ColorBlack { get; }
    }
}
