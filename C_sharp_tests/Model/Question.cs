﻿using C_sharp_tests.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_sharp_tests.Model
{
    public class Question
    {
        public const int NUM_OF_QUESTIONS = 10+1;

        private readonly int currentQuestion;
        private readonly string currentImage;
        private readonly string currenttextQuestion;
        private readonly string[] currenttextAnswerType;

        private string[] images =
        {
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\1.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\2.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\3.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\4.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\5.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\6.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\7.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\small.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\small.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\small.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\small.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\small.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\small.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\small.jpg",
            @"C:\Users\User\Documents\visual studio 2015\Projects\C_sharp_tests\C_sharp_tests\Resources\small.jpg",
        };

        private string[] textQuestions =
        {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15"
        };

        private string[,] textAnswerTypes=
        {
            { "1", "1.1", "1.1","1.1","1.1","1.1"},
            { "2", "2.1", "2.2","2.3","2.4","2.5"},
            { "3", "3.1", "3.2","3.3","3.4","3.5"},
            { "4", "4.1", "4.2","4.3","4.4","4.5"},
            { "5", "5.1", "5.2","5.3","5.4","5.5"},
            { "6", "6.1", "6.2","6.3","6.4","6.5"},
            { "7", "7.1", "7.2","7.3","7.4","7.5"},
            { "8", "8.1", "8.2","8.3","8.4","8.5"},
            { "9", "9.1", "9.2","9.3","9.4","9.5"},
            { "10", "10.1", "10.2","10.3","10.4","10.5"},
            { "11", "11.1", "11.2","11.3","11.4","11.5"},
            { "12", "12.1", "12.2","12.3","12.4","12.5"},
            { "13", "13.1", "13.2","13.3","13.4","13.5"},
            { "14", "14.1", "14.2","14.3","14.4","14.5"},
            { "15", "15.1", "15.2","15.3","15.4","15.5"}
        };


        public Question(int which)
        {
            currentQuestion = which;
            currentImage = images[which];
            currenttextQuestion = textQuestions[which];
            currenttextAnswerType = new string[6];
            currenttextAnswerType[0] = textAnswerTypes[which, 0];
            currenttextAnswerType[1] = textAnswerTypes[which, 1];
            currenttextAnswerType[2] = textAnswerTypes[which, 2];
            currenttextAnswerType[3] = textAnswerTypes[which, 3];
            currenttextAnswerType[4] = textAnswerTypes[which, 4];
            currenttextAnswerType[5] = textAnswerTypes[which, 5];
        }

        public string GetCurrentImage { get { return currentImage; } }
        public string GetCurrentQuestion { get { return currenttextQuestion; } }
        public int GetCurrentQuestionIndex { get { return currentQuestion; } }
        
        public string GetCurrentAnswer(int i)
        {
            return currenttextAnswerType[i];
        }
    }
}
